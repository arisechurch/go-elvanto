package v1_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"encoding/json"
	"gitlab.com/arisechurch/go-elvanto/pkg/api/v1"
	"gitlab.com/arisechurch/go-elvanto/pkg/elvanto"
	"net/http"
)

func testServiceService_Get(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		assert.Equal("/services/getInfo.json", r.URL.Path)
		assert.Equal("application/json", r.Header.Get("Content-type"))

		opt := &elvanto.ServiceGetOptions{}
		err := json.NewDecoder(r.Body).Decode(opt)
		assert.Nil(err)
		assert.Equal("1234-1234", string(*opt.Id))

		err = json.NewEncoder(w).Encode(&v1.ServiceGetResponse{
			JsonResultMeta: &v1.JsonResultMeta{},
			Service: []*elvanto.Service{{
				Id:   elvanto.NewId("1234-1234"),
				Name: elvanto.String("Sunday Morning"),
			}},
		})
		assert.Nil(err)

		handled = true
	}

	service, err := c.Service.Get(&elvanto.ServiceGetOptions{
		Id: elvanto.NewId("1234-1234"),
	})

	assert.True(handled)
	assert.Nil(err)
	assert.NotNil(service)
	assert.Equal("Sunday Morning", *service.Name)
}

func TestServiceService_Get(t *testing.T) {
	t.Run("OK", testServiceService_Get)
}

func TestServiceService_GetAll(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		handled = true

		assert.Equal("/services/getAll.json", r.URL.Path)

		opt := &elvanto.ServiceGetAllOptions{}
		err := json.NewDecoder(r.Body).Decode(opt)
		assert.Nil(err)

		assert.Equal("published", *opt.Status)
		assert.Equal("testing", opt.Fields[0])

		err = json.NewEncoder(w).Encode(&v1.ServiceListResponse{
			JsonResultMeta: &v1.JsonResultMeta{},
			Services: &elvanto.ServiceListResults{
				Services: []*elvanto.Service{{
					Id:   elvanto.NewId("a"),
					Name: elvanto.String("One"),
				}, {
					Id:   elvanto.NewId("b"),
					Name: elvanto.String("Two"),
				}},
			},
		})
		assert.Nil(err)
	}

	results, err := c.Service.GetAll(&elvanto.ServiceGetAllOptions{
		Status: elvanto.String("published"),
		Fields: []string{
			"testing",
		},
	})

	if !assert.True(handled) {
		return
	}

	assert.Nil(err)
	assert.NotNil(results)
	assert.NotNil(results.Services[1])
	assert.Equal("One", *results.Services[0].Name)
	assert.Equal("Two", *results.Services[1].Name)
}
