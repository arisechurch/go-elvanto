package v1

import (
	"errors"
	"gitlab.com/arisechurch/go-elvanto/pkg/elvanto"
)

var _ elvanto.ServiceService = &ServiceService{}

type ServiceService struct {
	client *Client
}

type ServiceGetResponse struct {
	*JsonResultMeta
	Service []*elvanto.Service `json:"service,omitempty"`
}

func (s *ServiceService) Get(
	opt *elvanto.ServiceGetOptions,
) (*elvanto.Service, error) {
	jsonRes := &ServiceGetResponse{}
	if err := s.client.PostWithOptions(
		"/services/getInfo.json",
		opt,
		jsonRes,
	); err != nil {
		return nil, err
	}

	if jsonRes.JsonResultMeta.Error != nil {
		return nil, errors.New(*jsonRes.JsonResultMeta.Error.Message)
	}

	return jsonRes.Service[0], nil
}

type ServiceListResponse struct {
	*JsonResultMeta
	Services *elvanto.ServiceListResults `json:"services,omitempty"`
}

func (s *ServiceService) GetAll(
	opt *elvanto.ServiceGetAllOptions,
) (*elvanto.ServiceListResults, error) {
	jsonRes := &ServiceListResponse{}
	if err := s.client.PostWithOptions(
		"/services/getAll.json",
		opt,
		jsonRes,
	); err != nil {
		return nil, err
	}

	if jsonRes.JsonResultMeta.Error != nil {
		return nil, errors.New(*jsonRes.JsonResultMeta.Error.Message)
	}

	return jsonRes.Services, nil
}
