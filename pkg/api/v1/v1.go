package v1

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"
)

const (
	BaseUrl = "https://api.elvanto.com/v1"
	Get     = "GET"
	Post    = "POST"
)

type JsonError struct {
	Code    *int    `json:"code"`
	Message *string `json:"message"`
}

type JsonResultMeta struct {
	GeneratedIn *string    `json:"generated_in"`
	Status      *string    `json:"status"`
	Error       *JsonError `json:"error,omitempty"`
}
type Client struct {
	httpClient *http.Client

	BaseUrl string
	ApiKey  string

	Person  *PersonService
	Service *ServiceService
}

func New(apiKey string) *Client {
	c := &Client{
		httpClient: &http.Client{
			Timeout: 60 * time.Second,
		},
		BaseUrl: BaseUrl,
		ApiKey:  apiKey,
	}

	c.Person = &PersonService{client: c}
	c.Service = &ServiceService{client: c}

	return c
}

func (c *Client) createUrl(path string) string {
	url := bytes.Buffer{}
	url.WriteString(c.BaseUrl)
	url.WriteString(path)

	return url.String()
}

func (c *Client) createRequest(
	method string,
	path string,
	body io.Reader,
) (*http.Request, error) {
	url := c.createUrl(path)

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	req.SetBasicAuth(c.ApiKey, "x")

	return req, nil
}

func (c *Client) MaybeSetBoolParam(vals *url.Values, key string, val *bool) {
	if val == nil {
		return
	}

	if *val {
		vals.Add(key, "true")
	} else {
		vals.Add(key, "false")
	}
}
func (c *Client) MaybeSetStringParam(vals *url.Values, key string, val *string) {
	if val != nil && *val != "" {
		vals.Add(key, *val)
	}
}

func (c *Client) Do(req *http.Request) (*http.Response, error) {
	res, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode >= 400 {
		defer res.Body.Close()
		return nil, errors.New(fmt.Sprintf("http error: %d", res.StatusCode))
	}

	return res, nil
}

func (c *Client) Get(path string) (*http.Request, error) {
	return c.createRequest(Get, path, nil)
}

func (c *Client) GetDo(path string) (*http.Response, error) {
	req, err := c.Get(path)
	if err != nil {
		return nil, err
	}

	return c.Do(req)
}

func (c *Client) Post(path string, body io.Reader) (*http.Request, error) {
	return c.createRequest(Post, path, body)
}

func (c *Client) PostDo(path string, body io.Reader) (*http.Response, error) {
	req, err := c.Post(path, body)
	if err != nil {
		return nil, err
	}

	return c.Do(req)
}

func (c *Client) PostWithOptions(path string, opt interface{}, result interface{}) error {
	body := &bytes.Buffer{}
	json.NewEncoder(body).Encode(opt)

	req, err := c.Post(path, body)
	if err != nil {
		return err
	}

	req.Header.Set("Content-type", "application/json")

	res, err := c.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if err := json.NewDecoder(res.Body).Decode(result); err != nil {
		return err
	}

	return nil
}
