package v1

import (
	"errors"
	"gitlab.com/arisechurch/go-elvanto/pkg/elvanto"
)

var _ elvanto.PersonService = &PersonService{}

type PersonService struct {
	client *Client
}

type PersonGetResponse struct {
	*JsonResultMeta
	Person []*elvanto.Person `json:"person,omitempty"`
}

func (ps *PersonService) GetCustom(opt *elvanto.PersonGetOptions, result interface{}) error {
	return ps.client.PostWithOptions(
		"/people/getInfo.json",
		opt,
		result,
	)
}

func (ps *PersonService) Get(opt *elvanto.PersonGetOptions) (*elvanto.Person, error) {
	jsonRes := &PersonGetResponse{}
	if err := ps.GetCustom(opt, jsonRes); err != nil {
		return nil, err
	}

	if jsonRes.JsonResultMeta.Error != nil {
		return nil, errors.New(*jsonRes.JsonResultMeta.Error.Message)
	}

	return jsonRes.Person[0], nil
}

type PersonListResponse struct {
	*JsonResultMeta
	People *elvanto.PersonListResults `json:"people,omitempty"`
}

func (ps *PersonService) SearchCustom(opt *elvanto.PersonSearchOptions, result interface{}) error {
	return ps.client.PostWithOptions(
		"/people/search.json",
		opt,
		result,
	)
}

func (ps *PersonService) Search(opt *elvanto.PersonSearchOptions) (*elvanto.PersonListResults, error) {
	jsonRes := &PersonListResponse{}
	if err := ps.SearchCustom(opt, jsonRes); err != nil {
		return nil, err
	}

	if jsonRes.JsonResultMeta.Error != nil {
		return nil, errors.New(*jsonRes.JsonResultMeta.Error.Message)
	}

	return jsonRes.People, nil
}

func (ps *PersonService) GetAllCustom(opt *elvanto.PersonGetAllOptions, result interface{}) error {
	return ps.client.PostWithOptions(
		"/people/getAll.json",
		opt,
		result,
	)
}

func (ps *PersonService) GetAll(opt *elvanto.PersonGetAllOptions) (*elvanto.PersonListResults, error) {
	jsonRes := &PersonListResponse{}
	if err := ps.GetAllCustom(opt, jsonRes); err != nil {
		return nil, err
	}

	if jsonRes.JsonResultMeta.Error != nil {
		return nil, errors.New(*jsonRes.JsonResultMeta.Error.Message)
	}

	return jsonRes.People, nil
}

type PersonEditResponse struct {
	*JsonResultMeta
	Person *elvanto.Person `json:"person,omitempty"`
}

func (ps *PersonService) Edit(opt *elvanto.PersonEditOptions) error {
	jsonRes := &PersonEditResponse{}
	if err := ps.client.PostWithOptions(
		"/people/edit.json",
		opt,
		jsonRes,
	); err != nil {
		return err
	}

	if jsonRes.JsonResultMeta.Error != nil {
		return errors.New(*jsonRes.JsonResultMeta.Error.Message)
	}

	return nil
}
