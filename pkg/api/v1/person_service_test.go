package v1_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"encoding/json"
	"gitlab.com/arisechurch/go-elvanto/pkg/api/v1"
	"gitlab.com/arisechurch/go-elvanto/pkg/elvanto"
	"net/http"
)

func testPersonService_Get(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		assert.Equal("/people/getInfo.json", r.URL.Path)
		assert.Equal("application/json", r.Header.Get("Content-type"))

		opt := &elvanto.PersonGetOptions{}
		err := json.NewDecoder(r.Body).Decode(opt)
		assert.Nil(err)
		assert.Equal("1234-1234", string(*opt.Id))

		err = json.NewEncoder(w).Encode(&v1.PersonGetResponse{
			JsonResultMeta: &v1.JsonResultMeta{},
			Person: []*elvanto.Person{{
				Id:        elvanto.NewId("1234-1234"),
				Firstname: elvanto.String("John"),
			}},
		})
		assert.Nil(err)

		handled = true
	}

	person, err := c.Person.Get(&elvanto.PersonGetOptions{
		Id: elvanto.NewId("1234-1234"),
	})

	assert.True(handled)
	assert.Nil(err)
	assert.NotNil(person)
	assert.Equal("John", *person.Firstname)
}

func TestPersonService_Get(t *testing.T) {
	t.Run("OK", testPersonService_Get)
}

func TestPersonService_Search(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		handled = true

		assert.Equal("/people/search.json", r.URL.Path)

		opt := &elvanto.PersonSearchOptions{}
		err := json.NewDecoder(r.Body).Decode(opt)
		assert.Nil(err)

		search := opt.Search.(map[string]interface{})
		assert.Equal("john", search["firstname"])

		err = json.NewEncoder(w).Encode(&v1.PersonListResponse{
			JsonResultMeta: &v1.JsonResultMeta{},
			People: &elvanto.PersonListResults{
				People: []*elvanto.Person{{
					Id:        elvanto.NewId("a"),
					Firstname: elvanto.String("One"),
				}, {
					Id:        elvanto.NewId("b"),
					Firstname: elvanto.String("Two"),
				}},
			},
		})
		assert.Nil(err)
	}

	results, err := c.Person.Search(&elvanto.PersonSearchOptions{
		Search: &elvanto.Person{
			Firstname: elvanto.String("john"),
		},
	})

	if !assert.True(handled) {
		return
	}

	assert.Nil(err)
	assert.NotNil(results)
	assert.NotNil(results.People[1])
	assert.Equal("One", *results.People[0].Firstname)
	assert.Equal("Two", *results.People[1].Firstname)
}

func TestPersonService_GetAll(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		handled = true

		assert.Equal("/people/getAll.json", r.URL.Path)

		opt := &elvanto.PersonGetAllOptions{}
		err := json.NewDecoder(r.Body).Decode(opt)
		assert.Nil(err)

		assert.Equal("testing", opt.Fields[0])
		assert.Equal("123", opt.Fields[1])

		err = json.NewEncoder(w).Encode(&v1.PersonListResponse{
			JsonResultMeta: &v1.JsonResultMeta{},
			People: &elvanto.PersonListResults{
				People: []*elvanto.Person{{
					Id:        elvanto.NewId("a"),
					Firstname: elvanto.String("One"),
				}, {
					Id:        elvanto.NewId("b"),
					Firstname: elvanto.String("Two"),
				}},
			},
		})
		assert.Nil(err)
	}

	results, err := c.Person.GetAll(&elvanto.PersonGetAllOptions{
		Fields: []string{
			"testing",
			"123",
		},
	})

	if !assert.True(handled) {
		return
	}

	assert.Nil(err)
	assert.NotNil(results)
	assert.NotNil(results.People[1])
	assert.Equal("One", *results.People[0].Firstname)
	assert.Equal("Two", *results.People[1].Firstname)
}

func TestPersonService_Edit(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		handled = true

		assert.Equal("/people/edit.json", r.URL.Path)

		opt := &elvanto.PersonEditOptions{}
		err := json.NewDecoder(r.Body).Decode(opt)
		assert.Nil(err)
		assert.Equal("a", string(*opt.Id))
		assert.Equal("New", *opt.Firstname)
		assert.NotNil(opt.Fields)

		fields := opt.Fields.(map[string]interface{})
		assert.Equal("123", fields["custom_test"])

		err = json.NewEncoder(w).Encode(&v1.PersonEditResponse{
			JsonResultMeta: &v1.JsonResultMeta{},
			Person: &elvanto.Person{
				Id: elvanto.NewId("a"),
			},
		})
		assert.Nil(err)
	}

	err := c.Person.Edit(&elvanto.PersonEditOptions{
		Person: &elvanto.Person{
			Id:        elvanto.NewId("a"),
			Firstname: elvanto.String("New"),
		},
		Fields: map[string]string{
			"custom_test": "123",
		},
	})

	if !assert.True(handled) {
		return
	}

	assert.Nil(err)
}
