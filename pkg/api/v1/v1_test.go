package v1_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	api "gitlab.com/arisechurch/go-elvanto/pkg/api/v1"
)

type Server struct {
	Server  *httptest.Server
	Handler *Handler
}

type Handler struct {
	http.Handler

	HandlerFn func(w http.ResponseWriter, r *http.Request)
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.HandlerFn(w, r)
}

func MustOpenServerClient() (*Server, *api.Client) {
	h := &Handler{}
	s := &Server{
		Server:  httptest.NewServer(h),
		Handler: h,
	}

	c := api.New("foobar")
	c.BaseUrl = s.Server.URL

	return s, c
}

func testV1_Client_GetDo(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(w http.ResponseWriter, r *http.Request) {
		handled = true
		fmt.Fprintf(w, "hello")

		assert.Equal("/test", r.URL.Path)

		user, pass, ok := r.BasicAuth()
		assert.True(ok)
		assert.Equal("foobar", user)
		assert.Equal("x", pass)
	}

	res, err := c.GetDo("/test")
	defer res.Body.Close()

	assert.True(handled)
	assert.Nil(err)
	assert.Equal(http.StatusOK, res.StatusCode)
	resBody, err := ioutil.ReadAll(res.Body)
	assert.Nil(err)
	assert.Equal("hello", string(resBody))
}

func testV1_Client_GetDo_Non200Status(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(w http.ResponseWriter, r *http.Request) {
		handled = true

		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "hello")
	}

	res, err := c.GetDo("/test")

	assert.True(handled)
	assert.NotNil(err)
	assert.Nil(res)
	assert.Equal("http error: 401", err.Error())
}

func TestV1_Client_GetDo(t *testing.T) {
	t.Run("OK", testV1_Client_GetDo)
	t.Run("Non200Status", testV1_Client_GetDo_Non200Status)
}

// Test `Get`
func testV1_ClientGet(t *testing.T) {
	assert := assert.New(t)

	c := api.New("foobar")
	req, err := c.Get("/test")

	assert.Nil(err)
	assert.NotNil(req)

	assert.Equal(
		"https://api.elvanto.com/v1/test",
		req.URL.String(),
	)
}

func TestV1_ClientGet(t *testing.T) {
	t.Run("OK", testV1_ClientGet)
}
