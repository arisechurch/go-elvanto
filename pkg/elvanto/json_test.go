package elvanto_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"gitlab.com/arisechurch/go-elvanto/pkg/elvanto"

	"encoding/json"
)

func testElvanto_Json_Song_UnmarshalJSON(t *testing.T) {
	assert := assert.New(t)

	song := &elvanto.Song{}
	err := json.Unmarshal([]byte("\"\""), song)
	assert.Nil(err)

	err = json.Unmarshal(
		[]byte("{\"id\":\"abc\"}"),
		song,
	)
	assert.Nil(err)
	assert.Equal("abc", string(*song.Id))
}

func TestElvanto_Json_Song(t *testing.T) {
	t.Run("Song UnmarshalJSON", testElvanto_Json_Song_UnmarshalJSON)
}

func testElvanto_Json_Volunteers_UnmarshalJSON(t *testing.T) {
	assert := assert.New(t)

	volunteers := &elvanto.Volunteers{}
	err := json.Unmarshal([]byte("\"\""), volunteers)
	assert.Nil(err)
}

func TestElvanto_Json_Volunteers(t *testing.T) {
	t.Run("Volunteers UnmarshalJSON", testElvanto_Json_Volunteers_UnmarshalJSON)
}

func testElvanto_Json_Service_UnmarshalJSON(t *testing.T) {
	assert := assert.New(t)

	service := &elvanto.Service{}
	err := json.Unmarshal([]byte(`{
		"name":"Service",
		"service_times": [],
		"rehearsal_times": [],
		"other_times": [],
		"plans": [],
		"volunteers": [],
		"songs": [],
		"files": [],
		"notes": []
	}`), service)
	assert.Nil(err)
}

func TestElvanto_Json_Service(t *testing.T) {
	t.Run("Service UnmarshalJSON", testElvanto_Json_Service_UnmarshalJSON)
}
