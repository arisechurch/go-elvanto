package elvanto_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"gitlab.com/arisechurch/go-elvanto/pkg/elvanto"

	"encoding/json"
	"time"
)

func createTime() time.Time {
	return time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC)
}

func createDate() time.Time {
	return time.Date(2009, time.November, 10, 0, 0, 0, 0, time.UTC)
}

func testElvanto_Time_Time_MarshalJSON(t *testing.T) {
	assert := assert.New(t)

	testTime := createTime()
	et := elvanto.Time(testTime)

	result, err := json.Marshal(et)
	assert.Nil(err)

	assert.Equal("\"2009-11-10 23:00:00\"", string(result))
}

func testElvanto_Time_Time_UnmarshalJSON(t *testing.T) {
	assert := assert.New(t)

	et := &elvanto.Time{}
	err := json.Unmarshal([]byte("\"2009-11-10 23:00:00\""), et)
	assert.Nil(err)

	testTime := time.Time(*et)
	assert.True(testTime.Equal(createTime()))
}

func testElvanto_Time_Date_MarshalJSON(t *testing.T) {
	assert := assert.New(t)

	testTime := createDate()
	ed := elvanto.Date(testTime)

	result, err := json.Marshal(ed)
	assert.Nil(err)

	assert.Equal("\"2009-11-10\"", string(result))
}

func testElvanto_Time_Date_UnmarshalJSON(t *testing.T) {
	assert := assert.New(t)

	ed := &elvanto.Date{}
	err := json.Unmarshal([]byte("\"2009-11-10\""), ed)
	assert.Nil(err)

	testTime := time.Time(*ed)
	assert.True(testTime.Equal(createDate()))
}

func TestElvanto_Time_Time(t *testing.T) {
	t.Run("MarshalJSON", testElvanto_Time_Time_MarshalJSON)
	t.Run("UnmarshalJSON", testElvanto_Time_Time_UnmarshalJSON)
}

func TestElvanto_Time_Date(t *testing.T) {
	t.Run("OK", testElvanto_Time_Date_MarshalJSON)
	t.Run("NoGroups", testElvanto_Time_Date_UnmarshalJSON)
}
