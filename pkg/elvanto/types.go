package elvanto

import (
	"time"
)

func NewId(id string) *Id {
	newId := Id(id)
	return &newId
}

func String(str string) *string {
	return &str
}

func Bool(b bool) *Boolean {
	value := Boolean(b)
	return &value
}

func Int(i int) *int {
	return &i
}

func NewTime(t time.Time) *Time {
	newTime := Time(t)
	return &newTime
}

func NewDate(t time.Time) *Date {
	newDate := Date(t)
	return &newDate
}
