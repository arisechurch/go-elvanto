package elvanto

import (
	"encoding/json"
	"fmt"
	"time"
)

// Check Time implements Marshaler & Unmarshaler
var _ json.Marshaler = &Time{}
var _ json.Unmarshaler = &Time{}

// Check Date implements Marshaler & Unmarshaler
var _ json.Marshaler = &Date{}
var _ json.Unmarshaler = &Date{}

const TimeFormat = "2006-01-02 15:04:05"
const DateFormat = "2006-01-02"

// Elvanto time format
func (t Time) MarshalJSON() ([]byte, error) {
	timeString := fmt.Sprintf(
		"\"%s\"",
		time.Time(t).Format(TimeFormat),
	)

	return []byte(timeString), nil
}

func (t *Time) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		return nil
	}

	var err error
	var newTime time.Time

	newTime, err = time.Parse(`"`+TimeFormat+`"`, string(data))
	if err != nil {
		return nil
	}

	*t = Time(newTime)
	return nil
}

// Elvanto date format
func (d Date) MarshalJSON() ([]byte, error) {
	timeString := fmt.Sprintf(
		"\"%s\"",
		time.Time(d).Format(DateFormat),
	)

	return []byte(timeString), nil
}

func (d *Date) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		return nil
	}

	var err error
	var newTime time.Time

	newTime, err = time.Parse(`"`+DateFormat+`"`, string(data))
	if err != nil {
		return nil
	}

	*d = Date(newTime)
	return nil
}
