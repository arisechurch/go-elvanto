package elvanto_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"gitlab.com/arisechurch/go-elvanto/pkg/elvanto"

	"encoding/json"
)

func testElvanto_Boolean_MarshalJSON(t *testing.T) {
	assert := assert.New(t)

	ebt := elvanto.Boolean(true)
	ebf := elvanto.Boolean(false)

	var err error
	resultTrue, err := json.Marshal(ebt)
	assert.Nil(err)
	resultFalse, err := json.Marshal(ebf)
	assert.Nil(err)

	assert.Equal("1", string(resultTrue))
	assert.Equal("0", string(resultFalse))
}

func testElvanto_Boolean_UnmarshalJSON(t *testing.T) {
	assert := assert.New(t)

	var ebt elvanto.Boolean
	var ebf elvanto.Boolean
	var err error
	err = json.Unmarshal([]byte("1"), &ebt)
	assert.Nil(err)
	err = json.Unmarshal([]byte("0"), &ebf)
	assert.Nil(err)

	assert.Equal(true, bool(ebt))
	assert.Equal(false, bool(ebf))
}

func TestElvanto_Boolean(t *testing.T) {
	t.Run("MarshalJSON", testElvanto_Boolean_MarshalJSON)
	t.Run("UnmarshalJSON", testElvanto_Boolean_UnmarshalJSON)
}

func testElvanto_YesNoBoolean_MarshalJSON(t *testing.T) {
	assert := assert.New(t)

	ebt := elvanto.YesNoBoolean(true)
	ebf := elvanto.YesNoBoolean(false)

	var err error
	resultTrue, err := json.Marshal(ebt)
	assert.Nil(err)
	resultFalse, err := json.Marshal(ebf)
	assert.Nil(err)

	assert.Equal("\"yes\"", string(resultTrue))
	assert.Equal("\"no\"", string(resultFalse))
}

func testElvanto_YesNoBoolean_UnmarshalJSON(t *testing.T) {
	assert := assert.New(t)

	var ebt elvanto.YesNoBoolean
	var ebf elvanto.YesNoBoolean
	var err error
	err = json.Unmarshal([]byte("\"yes\""), &ebt)
	assert.Nil(err)
	err = json.Unmarshal([]byte("\"no\""), &ebf)
	assert.Nil(err)

	assert.Equal(true, bool(ebt))
	assert.Equal(false, bool(ebf))
}

func TestElvanto_YesNoBoolean(t *testing.T) {
	t.Run("MarshalJSON", testElvanto_YesNoBoolean_MarshalJSON)
	t.Run("UnmarshalJSON", testElvanto_YesNoBoolean_UnmarshalJSON)
}
