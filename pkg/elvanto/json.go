package elvanto

import (
	"encoding/json"
)

type jsonUnmarshalService struct {
	*JsonService
	ServiceTimes   json.RawMessage `json:"service_times,omitempty"`
	RehearsalTimes json.RawMessage `json:"rehearsal_times,omitempty"`
	OtherTimes     json.RawMessage `json:"other_times,omitempty"`
	Plans          json.RawMessage `json:"plans,omitempty"`
	Volunteers     json.RawMessage `json:"volunteers,omitempty"`
	Songs          json.RawMessage `json:"songs,omitempty"`
	Files          json.RawMessage `json:"files,omitempty"`
	Notes          json.RawMessage `json:"notes,omitempty"`
}

func (s *Service) UnmarshalJSON(data []byte) error {
	results := &jsonUnmarshalService{}
	if err := json.Unmarshal(data, results); err != nil {
		return err
	}

	if results.ServiceTimes != nil &&
		string(results.ServiceTimes) != "[]" {
		results.JsonService.ServiceTimes = &ServiceTimes{}
		if err := json.Unmarshal(
			results.ServiceTimes,
			results.JsonService.ServiceTimes,
		); err != nil {
			return err
		}
	}
	if results.RehearsalTimes != nil &&
		string(results.RehearsalTimes) != "[]" {
		results.JsonService.RehearsalTimes = &RehearsalTimes{}
		if err := json.Unmarshal(
			results.RehearsalTimes,
			results.JsonService.RehearsalTimes,
		); err != nil {
			return err
		}
	}
	if results.OtherTimes != nil &&
		string(results.OtherTimes) != "[]" {
		results.JsonService.OtherTimes = &OtherTimes{}
		if err := json.Unmarshal(
			results.OtherTimes,
			results.JsonService.OtherTimes,
		); err != nil {
			return err
		}
	}
	if results.Plans != nil &&
		string(results.Plans) != "[]" {
		results.JsonService.Plans = &ServicePlans{}
		if err := json.Unmarshal(
			results.Plans,
			results.JsonService.Plans,
		); err != nil {
			return err
		}
	}
	if results.Volunteers != nil &&
		string(results.Volunteers) != "[]" {
		results.JsonService.Volunteers = &ServiceVolunteerPlans{}
		if err := json.Unmarshal(
			results.Volunteers,
			results.JsonService.Volunteers,
		); err != nil {
			return err
		}
	}
	if results.Songs != nil &&
		string(results.Songs) != "[]" {
		results.JsonService.Songs = &ServiceSongs{}
		if err := json.Unmarshal(
			results.Songs,
			results.JsonService.Songs,
		); err != nil {
			return err
		}
	}
	if results.Files != nil &&
		string(results.Files) != "[]" {
		results.JsonService.Files = &ServiceFiles{}
		if err := json.Unmarshal(
			results.Files,
			results.JsonService.Files,
		); err != nil {
			return err
		}
	}
	if results.Notes != nil &&
		string(results.Notes) != "[]" {
		results.JsonService.Notes = &ServiceNotes{}
		if err := json.Unmarshal(
			results.Notes,
			results.JsonService.Notes,
		); err != nil {
			return err
		}
	}

	*s = Service(*results.JsonService)

	return nil
}

// ====
// Fix empty strings "" used instead of objects or null

type jsonVolunteers Volunteers

func (v *Volunteers) UnmarshalJSON(data []byte) error {
	str := string(data)

	if str != "\"\"" {
		results := &jsonVolunteers{}
		if err := json.Unmarshal(data, results); err != nil {
			return err
		}

		*v = Volunteers(*results)
	}

	return nil
}

type jsonSong Song

func (s *Song) UnmarshalJSON(data []byte) error {
	str := string(data)

	if str != "\"\"" {
		results := &jsonSong{}
		if err := json.Unmarshal(data, results); err != nil {
			return err
		}

		*s = Song(*results)
	}

	return nil
}
