package elvanto

func (b Boolean) MarshalJSON() ([]byte, error) {
	value := bool(b)

	if value == true {
		return []byte("1"), nil
	}

	return []byte("0"), nil
}

func (b *Boolean) UnmarshalJSON(data []byte) error {
	*b = Boolean(string(data) == "1")
	return nil
}

func (b YesNoBoolean) MarshalJSON() ([]byte, error) {
	value := bool(b)

	if value == true {
		return []byte("\"yes\""), nil
	}

	return []byte("\"no\""), nil
}

func (b *YesNoBoolean) UnmarshalJSON(data []byte) error {
	*b = YesNoBoolean(string(data) == "\"yes\"")
	return nil
}
