package elvanto

import (
	"encoding/json"
	"time"
)

type Id string
type Time time.Time
type Date time.Time
type Boolean bool
type YesNoBoolean bool

// Person
type Person struct {
	Id                 *Id         `json:"id,omitempty"`
	Firstname          *string     `json:"firstname,omitempty"`
	MiddleName         *string     `json:"middle_name,omitempty"`
	Lastname           *string     `json:"lastname,omitempty"`
	PreferredName      *string     `json:"preferred_name,omitempty"`
	Email              *string     `json:"email,omitempty"`
	Phone              *string     `json:"phone,omitempty"`
	Mobile             *string     `json:"mobile,omitempty"`
	Birthday           *Date       `json:"birthday,omitempty"`
	Gender             *string     `json:"gender,omitempty"`
	Admin              *Boolean    `json:"admin,omitempty"`
	Archived           *Boolean    `json:"archived,omitempty"`
	Contact            *Boolean    `json:"contact,omitempty"`
	Volunteer          *Boolean    `json:"volunteer,omitempty"`
	Status             *string     `json:"status,omitempty"`
	Username           *string     `json:"username,omitempty"`
	LastLogin          *Time       `json:"last_login,omitempty"`
	Country            *string     `json:"country,omitempty"`
	Timezone           *string     `json:"timezone,omitempty"`
	Picture            *string     `json:"picture,omitempty"`
	FamilyId           *string     `json:"family_id,omitempty"`
	FamilyRelationship *string     `json:"family_relationship,omitempty"`
	CategoryId         *Id         `json:"category_id,omitempty"`
	Locations          []*Location `json:"locations,omitempty"`
	DateAdded          *Time       `json:"date_added,omitempty"`
	DateModified       *Time       `json:"date_modified,omitempty"`
}

// Location
type Location struct {
	Id   *Id     `json:"id,omitempty"`
	Name *string `json:"name,omitempty"`
}

// ====
// Person service things

// Person multiple results
type PaginationMeta struct {
	Page       *json.Number `json:"page"`
	PerPage    *json.Number `json:"per_page"`
	OnThisPage *json.Number `json:"on_this_page"`
	Total      *json.Number `json:"total"`
}

type PersonListResults struct {
	*PaginationMeta
	People []*Person `json:"person"`
}

// Person.Get
type PersonGetOptions struct {
	Id     *Id      `json:"id"`
	Fields []string `json:"fields,omitempty"`
}

// Person.Edit
type PersonEditOptions struct {
	*Person
	Fields interface{} `json:"fields,omitempty"`
}

// Person.Search
type PersonSearchOptions struct {
	Search   interface{} `json:"search"`
	Page     *int        `json:"page,omitempty"`
	PageSize *int        `json:"page_size,omitempty"`
	Fields   []string    `json:"fields,omitempty"`
}

// Person.GetAll
type PersonGetAllOptions struct {
	Page       *int          `json:"page,omitempty"`
	PageSize   *int          `json:"page_size,omitempty"`
	Fields     []string      `json:"fields,omitempty"`
	CategoryId []string      `json:"category_id,omitempty"`
	Suspended  *YesNoBoolean `json:"suspended,omitempty"`
	Contact    *YesNoBoolean `json:"contact,omitempty"`
	Archived   *YesNoBoolean `json:"archived,omitempty"`
}

// PersonService - interface
type PersonService interface {
	Get(opt *PersonGetOptions) (*Person, error)
	GetCustom(opt *PersonGetOptions, obj interface{}) error

	Edit(opt *PersonEditOptions) error

	Search(opt *PersonSearchOptions) (*PersonListResults, error)
	SearchCustom(opt *PersonSearchOptions, obj interface{}) error

	GetAll(opt *PersonGetAllOptions) (*PersonListResults, error)
	GetAllCustom(opt *PersonGetAllOptions, obj interface{}) error
}

// ====
// Services

// Service struct
type Service struct {
	Id             *Id                    `json:"id,omitempty"`
	Status         *int                   `json:"status,omitempty"`
	DateAdded      *Time                  `json:"date_added,omitempty"`
	DateModified   *Time                  `json:"date_modified,omitempty"`
	Name           *string                `json:"name,omitempty"`
	SeriesName     *string                `json:"series_name,omitempty"`
	Date           *Time                  `json:"date,omitempty"`
	Description    *string                `json:"description,omitempty"`
	ServiceType    *ServiceType           `json:"service_type,omitempty"`
	Location       *Location              `json:"location,omitempty"`
	ServiceTimes   *ServiceTimes          `json:"service_times,omitempty"`
	RehearsalTimes *RehearsalTimes        `json:"rehearsal_times,omitempty"`
	OtherTimes     *OtherTimes            `json:"other_times,omitempty"`
	Plans          *ServicePlans          `json:"plans,omitempty"`
	Volunteers     *ServiceVolunteerPlans `json:"volunteers,omitempty"`
	Songs          *ServiceSongs          `json:"songs,omitempty"`
	Files          *ServiceFiles          `json:"files,omitempty"`
	Notes          *ServiceNotes          `json:"notes,omitempty"`
}

type JsonService Service

type ServiceType struct {
	Id   *Id     `json:"id,omitempty"`
	Name *string `json:"name,omitempty"`
}

type ServiceTimes struct {
	Times []*ServiceTime `json:"service_time,omitempty"`
}

type RehearsalTimes struct {
	Times []*ServiceTime `json:"rehearsal_time,omitempty"`
}

type OtherTimes struct {
	Times []*ServiceTime `json:"other_time,omitempty"`
}

type ServicePlans struct {
	Plans []*ServicePlan `json:"plan,omitempty"`
}

type ServicePlan struct {
	TimeId                 *Id           `json:"time_id,omitempty"`
	ServiceLength          *int          `json:"service_length,omitempty"`
	ServiceLengthFormatted *string       `json:"service_length_formatted,omitempty"`
	TotalLength            *int          `json:"total_length,omitempty"`
	TotalLengthFormatted   *string       `json:"total_length_formatted,omitempty"`
	Items                  *ServiceItems `json:"items,omitempty"`
}

type ServiceItems struct {
	Items []*ServiceItem `json:"item,omitempty"`
}

type ServiceItem struct {
	Id          *Id      `json:"id,omitempty"`
	Heading     *Boolean `json:"heading,omitempty"`
	Duration    *string  `json:"duration,omitempty"`
	Title       *string  `json:"title,omitempty"`
	Description *string  `json:"description,omitempty"`
	Song        *Song    `json:"song,omitempty"`
}

type ServiceTime struct {
	Id           *Id     `json:"id,omitempty"`
	DateAdded    *Time   `json:"date_added,omitempty"`
	DateModified *Time   `json:"date_modified,omitempty"`
	Name         *string `json:"name,omitempty"`
	Starts       *Time   `json:"starts,omitempty"`
	Ends         *Time   `json:"ends,omitempty"`
}

type ServiceSongs struct {
	Songs []*Song `json:"song,omitempty"`
}

type Song struct {
	Id          *Id              `json:"id,omitempty"`
	Title       *string          `json:"title,omitempty"`
	Artist      *string          `json:"artist,omitempty"`
	Album       *string          `json:"album,omitempty"`
	CcliNumber  *string          `json:"ccli_number,omitempty"`
	Arrangement *SongArrangement `json:"arrangement,omitempty"`
}

type ServiceFiles struct {
	Files []*File `json:"file,omitempty"`
}

type File struct {
	Id      *Id      `json:"id,omitempty"`
	Title   *string  `json:"title,omitempty"`
	Type    *string  `json:"type,omitempty"`
	Html    *Boolean `json:"html,omitempty"`
	Content *string  `json:"content,omitempty"`
}

type ServiceNotes struct {
	Notes []*Note `json:"note,omitempty"`
}

type Note struct {
	Id           *Id     `json:"id,omitempty"`
	DateAdded    *Time   `json:"date_added,omitempty"`
	DateModified *Time   `json:"date_modified,omitempty"`
	Note         *string `json:"note,omitempty"`
}

type SongArrangement struct {
	Id       *Id     `json:"id,omitempty"`
	Title    *string `json:"title,omitempty"`
	Sequence *string `json:"sequence,omitempty"`
	Duration *string `json:"duration,omitempty"`
	Bpm      *string `json:"bpm,omitempty"`

	KeyId   *Id     `json:"key_id,omitempty"`
	KeyName *string `json:"key_name,omitempty"`
	Key     *string `json:"key,omitempty"`
}

type ServiceVolunteerPlans struct {
	Plans []*ServiceVolunteerPlan `json:"plan,omitempty"`
}

type ServiceVolunteerPlan struct {
	TimeId    *Id                 `json:"time_id,omitempty"`
	Positions *VolunteerPositions `json:"positions,omitempty"`
}

type VolunteerPositions struct {
	Positions []*VolunteerPosition `json:"position,omitempty"`
}

type VolunteerPosition struct {
	DepartmentId      *Id         `json:"department_id,omitempty"`
	DepartmentName    *string     `json:"department_name,omitempty"`
	SubDepartmentId   *Id         `json:"sub_department_id,omitempty"`
	SubDepartmentName *string     `json:"sub_department_name,omitempty"`
	PositionId        *Id         `json:"position_id,omitempty"`
	PositionName      *string     `json:"position_name,omitempty"`
	Volunteers        *Volunteers `json:"volunteers,omitempty"`
}

type Volunteers struct {
	Volunteers []*Volunteer `json:"volunteer,omitempty"`
}

type Volunteer struct {
	Person *Person `json:"person,omitempty"`
	Status *string `json:"status,omitempty"`
}

// ====
// Options

type ServiceGetOptions struct {
	Id     *Id      `json:"id,omitempty"`
	Fields []string `json:"fields,omitempty"`
}

type ServiceGetAllOptions struct {
	Page         *int          `json:"page,omitempty"`
	PageSize     *int          `json:"page_size,omitempty"`
	Status       *string       `json:"status,omitempty"`
	All          *YesNoBoolean `json:"all,omitempty"`
	ServiceTypes []*Id         `json:"service_types,omitempty"`
	Start        *Date         `json:"start,omitempty"`
	End          *Date         `json:"end,omitempty"`
	Fields       []string      `json:"fields,omitempty"`
}

type ServiceListResults struct {
	*PaginationMeta
	Services []*Service `json:"service,omitempty"`
}

// ServiceService - interface
type ServiceService interface {
	Get(opt *ServiceGetOptions) (*Service, error)
	GetAll(opt *ServiceGetAllOptions) (*ServiceListResults, error)
}
