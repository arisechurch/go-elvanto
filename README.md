# go-elvanto

[![GoDoc](https://godoc.org/gitlab.com/arisechurch/go-elvanto?status.svg)](https://godoc.org/gitlab.com/arisechurch/go-elvanto)

## Usage

```go
package main

import (
	"fmt"
	"gitlab.com/arisechurch/go-elvanto/pkg/elvanto"
	api "gitlab.com/arisechurch/go-elvanto/pkg/api/v1"
)

func main() {
	c := api.New("api_key_goes_here")

	person, _ := c.Person.Get(123, &elvanto.Options{})

	fmt.Printf("Hi, %s!", person.FirstName)
}
```
